# BINAR FSW5 Challenge-08 Kelompok 4  
 
## Running Testing
```sh
npm run start
```

## Running Eslint
```sh
npm run eslint
```

## Running Server
```sh
npm run start
```

## Link Deployment in Heroku 
root            : https://challenge8-fsw5-kelompok4.herokuapp.com/ <br>
documentation   : https://challenge8-fsw5-kelompok4.herokuapp.com/documentation <br>
get all cars    : https://challenge8-fsw5-kelompok4.herokuapp.com/v1/cars <br>
get cars by id  : https://challenge8-fsw5-kelompok4.herokuapp.com/v1/cars/:id <br>


